function reloadchat(message, clearChat) {
    var url = $(".btn-send-comment").data("url");
    var project_id = $(".btn-send-comment").data("project_id");
    $.ajax({
        url: url,
        type: "POST",
        data: {message: message, project_id: project_id},
        success: function (html) {
            if (clearChat == true) {
                $("#chat_message").val
                  var d = $('#chat-box');
            d.scrollTop(d.prop("scrollHeight"));
            }
            $("#chat-box").html(html);
          
        }
    });
}
setInterval(function () {
    reloadchat('', false);
}, 2000);
$(".btn-send-comment").on("click", function () {
    var message = $("#chat_message").val();
    reloadchat(message, true);
});
