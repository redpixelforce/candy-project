<?php

/* @var $this \yii\web\View */
/* @var $content string */

use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use app\assets\AppAsset;

AppAsset::register($this);
?>
<?php $this->beginPage() ?>
              <?php  Yii::$app->language = 'ru-RU'; ?>

<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
<!--    <link rel="stylesheet" type="text/css" href="style_navbar.css"/>-->
    <link rel="stylesheet" type="text/css" href="css/flipout_cards.css">
<!--  <link href="/web/css/bootstrap-material-design.css" rel="stylesheet">-->
<!--    <link href="/web/css/ripples.min.css" rel="stylesheet">-->
  <link href="/web/css/index.css" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="css/projects.css"/>
    <link rel="stylesheet" type="text/css" href="css/view-project.css"/>

    <?php $this->head() 
    
    ?>
    
</head>
<body>
 
<?php $this->beginBody() ?>

<div class="wrap">
       <?php
    NavBar::begin([
        'brandLabel' => 'Candy Project',
        'brandUrl' => Yii::$app->homeUrl,
        'options' => [
            'class' => 'navbar navbar-default',
        ],
    ]);
  $menuItems = [
    ['label' => 'Все проекты', 'url' => ['/site/index']],
    ['label' => 'О нас', 'url' => ['/site/about']],
    ['label' => 'Контакты', 'url' => ['/site/contact']],
];
 
if (Yii::$app->user->isGuest) {
    $menuItems[] = ['label' => 'Зарегистрироваться', 'url' => ['/site/registration']];
    $menuItems[] = ['label' => 'Войти', 'url' => ['/site/login']];
} else {
     $menuItems[] = ['label' => 'Мой проект', 'url' => ['/site/myproject']];
        
    $menuItems[] =    [
            'label' => 'Сообщения',
            'items' => [
                 ['label' => 'Входящие', 'url' => ['/message/message/inbox']],
                
        ['label' => 'Отправленные', 'url' => ['/message/message/sent']],
                 ['label' => 'Написать', 'url' => ['/message/message/compose']],
        ['label' => 'Игнорлист', 'url' => ['/message/message/ignorelist']],         
            ]
        ];
        $menuItems[] = '<li>'
        . Html::beginForm(['/site/logout'], 'post')
         . Html::submitButton(
            'Выйти (' . Yii::$app->user->identity->username . ')',
            ['class' => 'btn btn-link logout my-btn']
        )
        . Html::endForm()
        . '</li>';
}
 
echo Nav::widget([
    'options' => ['class' => 'navbar-nav navbar-right'],
    'items' => $menuItems,
]);
 
NavBar::end();
?>



    <div class="container">
        <?= Breadcrumbs::widget([
            'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
        ]) ?>
        <?= $content ?>
    </div>
</div>



<?php $this->endBody() ?>
</body>
</html>
<script>
function anichange (objName) {
 if ( $(objName).css('display') == 'none' ) {
 $(objName).animate({height: 'show'}, 400);
 } else {
 $(objName).animate({height: 'hide'}, 200);
 }
}
</script>
<?php $this->endPage() ?>
