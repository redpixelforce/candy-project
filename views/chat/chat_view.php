<div class="container">
    <div class="row">
        <div class="panel panel-default">
          <div class="panel-heading">Участники проекта</div>
          <div class="panel-body">
            <div class="container" id="chat-box">

            </div>
            <div class="panel-footer">
                 <div class="input-group">
                  <input type="text" class="form-control" id = "chat_message">
                  <span class="input-group-btn">
                    <button class="btn btn-default btn-send-comment" type="button" data-url  = "<?=$url?>" data-project_id = "<?=$project_id?>">Send</button>
                  </span>
                </div>
            </div>
          </div>
        </div>
    </div>
</div>