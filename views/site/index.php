<?php
use yii\widgets\LinkPager;
use Imagine\Image\Box;

use app\models\User;
?>
<style>

.card-project a {
    color: #000 !important;
    text-decoration: none !important;
}
</style>


       <h1 class="header-text">Проекты</h1>
   <div class="container-fluid">
    
         <?php for($i = 0; $i < count($projects); $i+=2) {?>
<div class="row">
                 <?php for($j = $i; $j < count($projects) && $j<$i+2; $j++) {?>

            <div class="col-md-5 col-md-offset-1" >
                <div class="well bs-component card-project">
                    <a href=<?=Yii::$app->getUrlManager()->createUrl(['site/viewproject','id' => $projects[$j]->id ])?>>
                    <div class="card-image border-tlr-radius">
                        
                        <img src="files/<?=$projects[$j]->image?>"></img>
                    </div>
                    <p class="title"><b><?=$projects[$j]->name?></b></p>
                    <?php
                        if ($projects[$j]->is_complete==0) {
                            ?><p class="status-inwork"><b>В процессе</b></p><?php
                        } else {
                            ?> <p class="status-all"><b>Закончен</b></p> <?php
                        }
                    ?>
                    <div class="text-project">
                    <p><?=$projects[$j]->short_describe?></p>
                 
                </div>
                    </a>
            </div>
            
       </div>

    <?php }?>
</div>

<?php }?>
     
</div>
<?= LinkPager::widget(['pagination' => $pagination]) ?>