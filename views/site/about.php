<?php

/* @var $this yii\web\View */

use yii\helpers\Html;

$this->title = 'About';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="site-about">
    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        Мы - площадка для развития проектов на базе Центра Развития Одаренных Детей, который находится в Калининградской области в поселке Ушаково. Данный сайт создан для тех, кто хочет рассказать больше о своем проекте окружающим, найти сторонников и экспертов. Сайт позволяет вести дневник проекта, отчитываться окружающим о том, на какой стадии находится проект. 
    </p>
    <p>Не забрасывайте свою мечту!</p>

</div>
