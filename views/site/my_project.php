<?php
use yii\helpers\Html;
use  yii\bootstrap\ActiveForm;
use app\models\ChatWidget;
?>
<style>
    .chat-container {
            padding-top: 5%;
                    padding-bottom: 5%;

    }
    .chat-text {
        text-align: center;
    }
</style>
<?php $f = ActiveForm::begin(['options' =>['enctype' => 'multipart/form-data']]); ?>

<?=$f->field($form, 'name')->label('Название проекта'); ?>

<?=$f->field($form, 'target')->label('Цель проекта'); ?>
<?=$f->field($form, 'problems')->textArea(['rows' => 4, 'enableLabel' => true])->label('Задачи проекта'); ?> 
<?=$f->field($form, 'shortDescribe')->textArea(['rows' => 4, 'enableLabel' => true])->label('Краткое описание проекта(не более 100 слов)'); ?> 
<?=$f->field($form, 'describe')->textArea(['rows' => 6, 'enableLabel' => true])->label('Описание'); ?> 
<?=$f->field($form, 'authors')->textArea(['rows' => 6, 'enableLabel' => true])->label('Авторы проекта'); ?>
<?=$f->field($form, 'contactMail')->label('Контактный e-mail проекта'); ?>
<?= $f->field($form, 'file')->fileInput()->label('Загрузить изображение'); ?>
<?=$f->field($form, 'linkVideo')->label('Видео проекта(ссылка на YouTube)'); ?>
 <?= $f->field($form, 'visibleMail')->checkbox()->label('Показывать пользователям вашу почту'); ?>
 <?= $f->field($form, 'visibleAuthor')->checkbox()->label('Показывать пользователям имя автора проекта'); ?>
<a></a>
<?=Html::submitButton('Сохранить',['class' => 'btn btn-primary', 'name' => 'login-button']); ?>
<?php ActiveForm::end(); ?>
<h4>Дневник разработки</h4>
<br>
<p>Дневник разработки нужен для рассказа о каждом этапе развития проекта. Каждую неделю добавляйте новую запись в дневник разработки для рассказа, что вы успели за нее сделать и что планируете делать дальше</p>
<a style = "margin: 5px"  href= <?=Yii::$app->getUrlManager()->createUrl(['site/newweek'])?>>Добавить новую запись в дневник разработки</a> 
<br>
<a style = "margin: 5px"  href= <?=Yii::$app->getUrlManager()->createUrl(['site/viewweeks'])?>>Посмотреть все недели проекта</a> 
<br>
<h4>Временная линия</h4>
<br>
<p>Временная линия показывает, что и к какому сроку вы планируете сделать</p>
<a style = "margin: 5px"  href= <?=Yii::$app->getUrlManager()->createUrl(['site/newlabel'])?>>Добавить новую метку на временную линию</a> 
<br>
<a style = "margin: 5px"  href= <?=Yii::$app->getUrlManager()->createUrl(['site/viewlabels'])?>>Посмотреть все метки проекта</a> 
<br>
<div class="container chat-container">
<div class="row">
    
    <div class="col-md-12 chat-text"><h3>Чат участников проекта</h3></div>    
</div>
<?= ChatWidget::widget() ?>
    
    </div>

