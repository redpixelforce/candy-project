<?php
use yii\helpers\Html;
use  yii\bootstrap\ActiveForm;
   
use kartik\date\DatePicker;
?>

<?php $f = ActiveForm::begin(); ?>
<?=$f->field($form, 'title')->textInput(['enableLabel' => true])->label('Название метки'); ?>

<?=$f->field($form, 'description')->textArea(['rows' => 3, 'enableLabel' => true])->label('Опишите, что к этому времени должно быть готово'); ?>
<?=$f->field($form, 'date1')->widget(DatePicker::classname(), [
    'options' => ['placeholder' => 'Вставьте дату для метки'],
    'pluginOptions' => [
        'autoclose'=>true,
        'format' => 'dd-mm-yyyy'

    ]
]); ?>

<?=Html::submitButton('Сохранить',['class' => 'btn btn-primary', 'name' => 'login-button']); ?>
<?php ActiveForm::end(); ?>

