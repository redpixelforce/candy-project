<?php
use yii\helpers\Html;
use  yii\bootstrap\ActiveForm
   
?>

<?php $f = ActiveForm::begin(['options' =>['enctype' => 'multipart/form-data']]); ?>
<?=$f->field($form, 'titleWeek')->textInput(['enableLabel' => true])->label('Название недели'); ?>

<?=$f->field($form, 'newMaterial')->textArea(['rows' => 6, 'enableLabel' => true])->label('Опишите, что в проект за неделю было добавлено нового'); ?>
<?=$f->field($form, 'plannedMaterial')->textArea(['rows' => 6, 'enableLabel' => true])->label('Опишите, что вы планируете добавить в проект на следующей неделе'); ?>
<?=$f->field($form, 'imageMaterial[]')->fileInput(['multiple' => true])->label('Загрузить изображения(максимум 4 штуки)'); ?>

<?=Html::submitButton('Сохранить',['class' => 'btn btn-primary', 'name' => 'login-button']); ?>
<?php ActiveForm::end(); ?>
