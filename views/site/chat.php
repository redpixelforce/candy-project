<?php
use yii\widgets\LinkPager;
use Imagine\Image\Box;

use app\models\User;
?>
<?php echo \sintret\chat\ChatRoom::widget([
            'url' => \yii\helpers\Url::to(['/chat/send-chat']),
            'userModel'=>  \app\models\User::className(),
            'userField' => 'avatarImage'
]); ?>