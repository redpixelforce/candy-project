<?php

use yii\widgets\LinkPager;
use app\models\DateManager; 



?>
<style>
.video-container {
	position:relative;
	padding-bottom:56.25%;
	padding-top:30px;
	height:0;
	overflow:hidden;
}

.video-container iframe, .video-container object, .video-container embed {
	position:absolute;
	top:0;
	left:0;
	width:100%;
	height:100%;
}
    .nav-tabs {
        
        margin: 10%;
    }
</style>
	<link rel="stylesheet" href="css/reset.css"> <!-- CSS reset -->
	<link rel="stylesheet" href="css/style.css"> <!-- Resource style -->
<h1 class="header-text">Проект: <?=$project->name?></h1>
<div class="container-fluid">
     <div class="row">
           <div class="col-md-offset-1 col-md-10 col-md-offset-1">
       <div class="img-card">
 <img src="files/<?=$project->image?>"></img>
</div>
    </div>
</div>
<ul class="nav nav-tabs">
  <li class="active"><a data-toggle="tab" href="#panel1">О проекте</a></li>
  <li><a data-toggle="tab" href="#panel2">Дневник разработки</a></li>
  <li><a data-toggle="tab" href="#panel3">Таймлайн</a></li>
</ul>
 </div>
<div class="tab-content">
  <div id="panel1" class="tab-pane fade in active">
    <h3 style="text-align:center">О проекте</h3>
      <div class="container-fluid">
      <div class="row">
        <h3 style="text-align:center">Цель</h4>
           <div class="col-md-offset-1 col-md-10 col-md-offset-1">
            <div class="card">
             <div class="container">
                 
                <?=nl2br($project->target)?>
                </div>
             </div> 
           </div>
    </div>
 <div class="row">
        <h3 style="text-align:center">Задачи</h4>
           <div class="col-md-offset-1 col-md-10 col-md-offset-1">
            <div class="card">
             <div class="container">
                 
                <?=nl2br($project->problems)?>
                </div>
             </div> 
           </div>
    </div>
       <div class="row">
        <h3 style="text-align:center">Описание</h4>
           <div class="col-md-offset-1 col-md-10 col-md-offset-1">
            <div class="card">
             <div class="container">
                 
                <?=nl2br($project->describe_project)?>
                </div>
             </div> 
           </div>
    </div>
<?php if ($project->show_author==1) {?>
  <div class="row">
        <h3 style="text-align:center">Авторы проекта</h4>
           <div class="col-md-offset-1 col-md-10 col-md-offset-1">
            <div class="card">
             <div class="container">
                 
                <?=nl2br($project->authors)?>
                </div>
             </div> 
           </div>
    </div>
<?php }?>
<?php if ($project->show_mail==1) {?>
  <div class="row">
        <h3 style="text-align:center">E-mail проекта</h4>
           <div class="col-md-offset-1 col-md-10 col-md-offset-1">
            <div class="card">
             <div class="container">
                 
                <?=nl2br($project->contact_mail)?>
                </div>
             </div> 
           </div>
    </div>
<?php }?>
<?php if ($project->video_project!="") {?>
<div class="row" id="frame_row">
        <h3 style="text-align:center">Видео проекта</h4>
           <div class="col-md-offset-1 col-md-10 col-md-offset-1">
               <div class="video-container">
                   <iframe src="https://www.youtube.com/embed/<?=$project->video_project?>" frameborder="0" allowfullscreen></iframe>
               </div>
           </div>
    </div>
<?php } ?>
      
      </div>
  </div>
  <div id="panel2" class="tab-pane fade">
    <div class="container">
        <h3 style="text-align:center">Дневник разработки</h4>
    <p style="text-align:center">
    Дневник разработки показывает, как продвинулась разработка проекта за каждую неделю
    </p>
<?php for ($i = 0; $i < count($weeks); $i++) { ?>
  <div class="row">
<div class="col-md-offset-1 col-md-10 col-md-offset-1">
<div class="card">
    <div class="container">
        <a href = "#divId<?=$i?>"><h3 href="#" onclick="anichange('#divId<?=$i ?>'); return false"><?=$weeks[$i]->title_week?></h3></a>
    <div id="divId<?=$i?>" style="display: none">
<h4>Что добавлено нового на этой неделе</h4>
        <br>
        <p>
        <?=nl2br($weeks[$i]->new_data) ?>
        </p>
        <br>
<h4>Что планируется добавить нового</h4>
        <br> 
        <p>
        <?=nl2br($weeks[$i]->expected_data) ?>
        </p>
        <br>
        <?php if ($weeks[$i]->image_week != "") { ?>
        <h4>Изображения:</h4>
        <?php $images = explode(",", $weeks[$i]->image_week); ?>
        <?php for ($j = 0; $j< count($images)-1; $j++) { ?>
        <a href = "week_image/<?=$images[$j]?>">Изображение №<?=$j+1 ?></a>
        <br>
        <?php } ?>
    <?php } ?>
        </div> 
    </div>
    
    </div>
</div>
    </div>
<?php } ?>
</div>
<?= LinkPager::widget(['pagination' => $pagination]) ?>
  </div>
    
  <div id="panel3" class="tab-pane fade">
    <?php if (count($labels)!=0) { ?>
    <section class="cd-horizontal-timeline">
	<div class="timeline">
		<div class="events-wrapper">
			<div class="events">
				<ol>
					<li><a href="#0" data-date="<?=$labels[0]->date?>" class="selected"><?=DateManager::toShortDate($labels[0]->date)?></a></li>
                    <?php for ($i = 1; $i < count($labels); $i++) { ?>

					<li><a href="#0" data-date="<?=$labels[$i]->date?>"><?=DateManager::toShortDate($labels[i]->date)?></a></li>
								<?php } ?>

				</ol>

				<span class="filling-line" aria-hidden="true"></span>
			</div> <!-- .events -->
		</div> <!-- .events-wrapper -->
			
		<ul class="cd-timeline-navigation">
			<li><a href="#0" class="prev inactive">Prev</a></li>
			<li><a href="#0" class="next">Next</a></li>
		</ul> <!-- .cd-timeline-navigation -->
	</div> <!-- .timeline -->

	<div class="events-content">
		<ol>
            <li class="selected" data-date="<?=$labels[0]->date?>">
				<h2><?=$labels[0]->title ?></h2>
				<em><?=DateManager::toLongDate($labels[0]->date)?></em>
				<p>	
					 <?=nl2br($labels[0]->description)?>
				</p>
			</li>
            <?php for ($i = 1; $i < count($labels); $i++) { ?>
			<li data-date="<?=$labels[$i]->date?>">
				<h2><?=$labels[$i]->title ?></h2>
				<em><?=DateManager::toLongDate($labels[$i]->date)?></em>
				<p>	
					 <?=nl2br($labels[$i]->description)?>
				</p>
			</li>

			<?php } ?>
		</ol>
	</div> <!-- .events-content -->
</section>
      <?php } ?>
  </div>
</div>





