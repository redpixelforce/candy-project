<?php
use yii\widgets\LinkPager;
use Imagine\Image\Box;

use app\models\User;
?>
<style>

.week-card a {
    color: #000 !important;
    text-decoration: none !important;
}
</style>
<div class="container-fluid">
<?php
    for ($i = 0; $i <count($weeks); $i++) {
    ?>
    <div class="row">
           <div class="col-md-offset-1 col-md-10 col-md-offset-1">
            <div class="card week-card">
                <a href=<?=Yii::$app->getUrlManager()->createUrl(['site/editweek','id' =>$weeks[$i]->project_id,'number_week' => $weeks[$i]->number_week ])?>>
             <div class="container">
                <h4>
                 Неделя #<?=$weeks[$i]->number_week ?>. <?=$weeks[$i]->title_week ?>
                 
                 </h4> 
                
                </div>
                </a>
             </div> 
           </div>
    </div>
    <?php
    }
    ?>
   
  
<?= LinkPager::widget(['pagination' => $pagination]) ?> 
</div>
