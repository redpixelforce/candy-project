<?php
use yii\helpers\Html;
use  yii\bootstrap\ActiveForm
   
?>
<?php $f = ActiveForm::begin(); ?>
<!--<?= $f->errorSummary($form); ?>-->


<?=$f->field($form, 'key1')->label('Ключ для регистрации'); ?>
<?=$f->field($form, 'name')->label('Имя пользователя'); ?>
<?=$f->field($form, 'email')->label('E-mail'); ?>
<?=$f->field($form, 'password')->passwordInput()->label('Пароль'); ?>
<?=$f->field($form, 'password_repeat')->passwordInput()->label('Повторите пароль'); ?>
<?=Html::submitButton('Зарегистрироваться',['class' => 'btn btn-primary', 'name' => 'login-button']); ?>

<?php ActiveForm::end(); ?>