<?php

namespace app\controllers;
use yii\helpers\Html;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\UploadedFile;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\SenderForm;
use app\models\MyProjectForm;
use app\models\ContactForm;
use app\models\NewLabelForm;
use app\models\RegisterForm;
use app\models\NewWeekForm;
use app\models\Userkeys;
use app\models\Labels;
use app\models\DateManager;
use app\models\Project_weeks;
use yii\imagine\Image;


use app\models\User;
use app\models\Projects;
use yii\data\Pagination;
class SiteController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout','login', 'signup','myproject','viewweeks','newweek','editweek'],
                'rules' => [
                    [
                        'actions' => ['logout','myproject','viewweeks','newweek','editweek'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
             [
                        'allow' => true,
                        'actions' => ['login', 'signup'],
                        'roles' => ['?'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
        Yii::$app->language = 'ru-RU';
        $projects = Projects::find();
        $pagination = new Pagination([
            'defaultPageSize' => 6,
            'totalCount'=>$projects->count()
        ]);
        $projects = $projects->offset($pagination->offset)->limit($pagination->limit)->all();
        return $this->render('index', [
            'projects' => $projects,
            'pagination' => $pagination
        ]);
    }

    /**
     * Login action.
     *
     * @return string
     */
    public function actionLogin()
    {        
        Yii::$app->language = 'ru-RU';

        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->goBack();
        }
        return $this->render('login', [
            'model' => $model,
        ]);
    }
 public function actionViewweeks()
    {
        $weeks =  Project_weeks::find()
    ->where(['project_id' => Yii::$app->user->identity->project_id]);
        $pagination = new Pagination([
            'defaultPageSize' => 6,
            'totalCount' => $weeks->count()
        ]);
        $weeks = $weeks->offset($pagination->offset)->limit($pagination->limit)->all();
        return $this->render('viewweek', [
            'weeks' => $weeks,
            'pagination' => $pagination
        ]);
    }
    /**
     * Logout action.
     *
     * @return string
     */
    public function actionLogout()
    {

        Yii::$app->user->logout();

        return $this->goHome();
    }
    public function actionEditweek() {

        $request = Yii::$app->request;
        $id = $request->get('id');  
        $number_week = $request->get('number_week');
        $week = Project_weeks::findOne([
            'project_id' => $id,
            'number_week' => $number_week
        ]);
        $form = new NewWeekForm();
        $form->loadData($week);
        $week_image = "";
        if ($form->load(Yii::$app->request->post())) {
            if ($form->validate()) {
                $form->imageMaterial = UploadedFile::getInstances($form, 'imageMaterial');
                foreach ($form->imageMaterial as $file) {
                    $name = uniqid() . '.' . $file->extension;
                    $file->saveAs('week_image/' .   $name . '.');
                    $week_image = $week_image.$name.','; 
                }
                if ($week_image!="") {
                    $week->image_week = $week_image;
                }
                $countWeek = $this->countWeeks(Yii::$app->user->identity->project_id); 
                $week->project_id = Yii::$app->user->identity->project_id;
                $week->number_week=$countWeek+1;
                $form->saveData($week);
                return $this->render('about');
            }
            else {
           
        }
            } 
        return $this->render('newweek',['form'=> $form]);
    }
      public function actionNewlabel() {

       $id =  Yii::$app->user->identity->project_id;
        $form = new NewLabelForm();
      
        if ($form->load(Yii::$app->request->post())) {
            if ($form->validate()) {
                $label = new Labels();
                $label->project_id = $id;
                $form->saveData($label);
                return $this->render('about');
            }
            else {
           
        }
            } 
        return $this->render('newlabel',['form'=> $form]);
    }
  public function actionViewproject()
    {

        $request = Yii::$app->request;
        $id = $request->get('id');  
        $project = Projects::findOne([
        'id' => $id,
        ]);
        $weeks =  Project_weeks::find()
        ->where(['project_id' => $id]);
        $labels =  Labels::find()->where(['project_id' => $id])->all();;
        $pagination = new Pagination([
            'defaultPageSize' => 3,
            'totalCount'=>$weeks->count()
        ]);
        $weeks = $weeks->offset($pagination->offset)->limit($pagination->limit)->all();
        $labels = DateManager::sortDates($labels);
        return $this->render('view_project',[
            'project' => $project,
            'weeks' => $weeks,
            'pagination'=> $pagination,
            'labels' => $labels
        ]);
 
    }
    /**
     * Displays contact page.
     *
     * @return string
     */
    public function actionContact()
    {

        $model = new ContactForm();
        if ($model->load(Yii::$app->request->post()) && $model->contact(Yii::$app->params['adminEmail'])) {
            Yii::$app->session->setFlash('contactFormSubmitted');

            return $this->refresh();
        }
        return $this->render('contact', [
            'model' => $model,
        ]);
    }

    /**
     * Displays about page.
     *
     * @return string
     */
public function actionChat() {

               return $this->render('chat');

}
    public function actionAbout()
    {

        return $this->render('about');
    }
    public function actionRegistration()
    {

      $form = new RegisterForm();  
        if ($form->load(Yii::$app->request->post()) && $form->validate()) {
        //сохранение данных в бд 
           $form->registrationUser();
           return $this->render('about');

        }
            
         else {
        
        }
        return $this->render('registration', [
            'form' => $form,
     
        ]);
    }
    
//    public function actionSendproject() {
//        $form = new SenderForm();
//        $form->file = UploadedFile::getInstance($form,'file');
//
//        if ($form->load(Yii::$app->request->post()) && $form->validate()) {
//            $form->file = UploadedFile::getInstance($form,'file');
//            $form->file->saveAs(Yii::$app->params['pathUploads'] .'files/'.$form->file->baseName.".".$form->file->extension);
//        } 
//        return $this->render('send_project',['form'=> $form]);
//    }
    private function countWeeks($project_id) {

        return  Project_weeks::find()
    ->where(['project_id' => $project_id])
    ->count();
    }   
    
    public function actionNewweek() {

        $form = new NewWeekForm();
         $week = new Project_weeks();
        $week->image_week ="";
            if ($form->load(Yii::$app->request->post())) {
                if ($form->validate()) {
                     $form->imageMaterial = UploadedFile::getInstances($form, 'imageMaterial');

                  foreach ($form->imageMaterial as $file) {
                      $name = uniqid() . '.' . $file->extension;
                $file->saveAs(Yii::$app->params['pathUploads'] . 'week_image/' .   $name . '.');
                    $week->image_week = $week->image_week.$name.','; 
            }
               
                $countWeek = $this->countWeeks(Yii::$app->user->identity->project_id); 
                $week->project_id = Yii::$app->user->identity->project_id;
                $week->number_week=$countWeek+1;
                $form->saveData($week);
                        return $this->render('about');
                }
                 else {
           
        }
        }
        return $this->render('newweek',['form'=> $form]);
    }
     public function actionViewlabels() {
          
         $labels =  Labels::find()
    ->where(['project_id' => Yii::$app->user->identity->project_id]);
        $pagination = new Pagination([
            'defaultPageSize' => 6,
            'totalCount' => $labels->count()
        ]);
        $labels = $labels->offset($pagination->offset)->limit($pagination->limit)->all();
        return $this->render('viewlabels', [
            'labels' => $labels,
            'pagination' => $pagination
        ]);
        
    }   
    public function actionEditlabel() {
         $request = Yii::$app->request;
        $id = $request->get('id');  
        $label = Labels::findOne([
            'id' => $id,
        ]);
        $form = new NewLabelForm();
        
        $form->loadData($label);
        if ($form->load(Yii::$app->request->post())) {
            if ($form->validate()) {
                
                $form->saveData($label);
                return $this->render('about');
            }
            else {
           
        }
            } 
        return $this->render('newlabel',['form'=> $form]);
    }
    public function actionProfile() {

        $users = Userkeys::find();
        $pagination = new Pagination([
            'defaultPageSize' => 2,
            'totalCount'=>$users->count()
        ]);
        $users = $users->offset($pagination->offset)->limit($pagination->limit)->all();
        return $this->render('profile', [
            'users'=>$users,
            'pagination'=>$pagination                            
        ]);
    }

    public function actionMyproject() {

           $form = new MyProjectForm();  
         $project = Projects::findOne([
        'id' => Yii::$app->user->identity->project_id,
]);
        
        
        $form->loadData($project); 
         if ($form->load(Yii::$app->request->post())) {
            $form->file = UploadedFile::getInstance($form,'file');
             if ($form->validate()) {
                 $name_image = uniqid().'.';
                $path = Yii::$app->params['pathUploads'] . 'files/'.$name_image;
                 if ($form->file!=null) {
                $form->file->saveAs($path.$form->file->extension);
                 $name_image = $name_image.$form->file->extension;
                 $project->image = $name_image;
                 }
                $form->saveData($project);
                return $this->render('about');
             }
         }
        return $this->render('my_project',['form'=> $form,'project'=>$project]);
    }
    public function actionKey() {

        $is_used = Yii::$app->request->get("is_used");
        $session = Yii::$app->session;
        $session->set("is_used",$is_used);
        return $this->render('key',['is_used' => Yii::$app->session->get("is_used")]);
    }
}
