<? 
namespace app\models;

use yii\base\Widget;
use yii\helpers\Html;
use app\models\User;
use app\models\Chat;
use yii\web\Request;
use Yii;
class ChatWidget extends Widget {
    public $sourcePath = '@app/web/';
    public $css = [
    ];
    public $js = [ // Configured conditionally (source/minified) during init()
        'js/chat.js',
    ];
    public $depends = [
        'yii\web\JqueryAsset',
    ];

    private $messages;
    public function init()
    {
        parent::init();
        $messages = Chat::find()
        ->where(['project_id' => Yii::$app->user->identity->project_id]);
    }

    public function run()
    {
    return $this->render('chat_view', [
            'messages' => $messages,
            'url' => \yii\helpers\Url::to(['/chat/send-chat']),
            'project_id' => Yii::$app->user->identity->project_id
        ]);
    }
    
    public function getViewPath()
{
    return Yii::getAlias('@app/views/chat');
}
    
    public static function sendChat($project_id, $message) {
        $model = new Chat();

        if ($message!=null) {
        $model->userId = Yii::$app->user->identity->id;
        $model->project_id = $project_id;
        $model->message = $message;
        if ($model->save()) {
                
                echo $model->getData();
            } else {
                print_r($model->getErrors());
                exit(0);
            }
        }
        echo $model->getData();
    }
}