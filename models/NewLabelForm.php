<?php

namespace app\models;


use Yii;
use yii\helpers\Html;
use yii\base\Model;

/**
 * LoginForm is the model behind the login form.
 *
 * @property User|null $user This property is read-only.
 *
 */
class NewLabelForm extends Model
{
    public $title;
    public $date1;
    public $description;
    

    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [
            // username and password are both required
            [['title','date1','description' ], 'required'],

    
            // rememberMe must be a boolean value
          
            
        ];
    }

    public function loadData($label) {
         $this->title = $label->title;
        $this->date1 = $label->date;
        $this->description =$label->description;
    }
    public function saveData($label) {
        $label->title = Html::encode($this->title);
        $label->date = $this->date1;
        $label->description = Html::encode($this->description);
        $label->save();
        
    }  
}