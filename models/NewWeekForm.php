<?php

namespace app\models;


use Yii;
use yii\helpers\Html;
use yii\base\Model;

/**
 * LoginForm is the model behind the login form.
 *
 * @property User|null $user This property is read-only.
 *
 */
class NewWeekForm extends Model
{
    public $titleWeek;
    public $newMaterial;
    public $plannedMaterial;
    public $imageMaterial;
    

    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [
            // username and password are both required
            [['newMaterial','plannedMaterial','titleWeek' ], 'required'],
              [['imageMaterial'],'image', 'extensions' => 'png, jpg','maxFiles' => 4

    ],
            // rememberMe must be a boolean value
          
            
        ];
    }
 public function getYoutubeIdFromUrl($url) {
    $parts = parse_url($url);
    if(isset($parts['query'])){
        parse_str($parts['query'], $qs);
        if(isset($qs['v'])){
            return $qs['v'];
        }else if(isset($qs['vi'])){
            return $qs['vi'];
        }
    }
    if(isset($parts['path'])){
        $path = explode('/', trim($parts['path'], '/'));
        return $path[count($path)-1];
    }
    return false;
}
    public function loadData($project_week) {
         $this->newMaterial = $project_week->new_data;
        $this->plannedMaterial = $project_week->expected_data;
        $this->titleWeek =$project_week->title_week;
    }
    public function saveData($project_week) {
        $project_week->title_week = Html::encode($this->titleWeek);
        $project_week->new_data = Html::encode($this->newMaterial);
        $project_week->expected_data = Html::encode($this->plannedMaterial);
        $project_week->save();
        
    }  
}