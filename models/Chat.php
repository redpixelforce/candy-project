<?

namespace app\models;
use Yii;
/**
 * This is the model class for table "chat".
 *
 * @property integer $id
 * @property string $message
 * @property integer $userId
 * @property string $updateDate
 */
class Chat extends \yii\db\ActiveRecord {

    public static function records() {
        
        return static::find()->where(['project_id' => Yii::$app->user->identity->project_id])->all();
    }
    
    public function getData() {
        $models = Chat::records();
        $output = '';
        foreach ($models as $model) {
            $username =  User::findOne(['id' => $model->userId ])->username;
            $output .= '<div class="row message-bubble">
                    <p class="text-muted">'.$username.'</p>
                    <p>'.$model->message.'</p>
                </div>'; 
        }
        return $output;

    }

}
