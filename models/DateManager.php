<?php

namespace app\models;
class DateManager {
    
    
    

private static $monthes = ['Январь','Февраль','Март','Апрель','Май','Июнь','Июль','Август','Сентябрь','Октябрь','Ноябрь','Декабрь'];
private static $shortMonthes = ['Янв.','Фев.','Мар.','Апр.','Май','Июнь','Июль','Авг.','Сент.','Окт.','Нояб.','Дек.'];
    

public static function toLongDate($dateString)
{
    $day =  substr($dateString,0,2);
    $month = (int) substr($dateString, 3,2);
    $year =  substr($dateString,6,4);
    $longDate = $day.' '.self::$monthes[$month-1].' '.$year;
    return $longDate;
}

public static function toShortDate($dateString)
{
    $day =  substr($dateString,0,2);
    $month = (int) substr($dateString, 3,2);
    $year = (int) substr($dateString,6,4);
    $shortDate = $day.' '.self::$shortMonthes[$month-1];
    return $shortDate;
}
public static function sortDates($dates) {
    usort($dates, array(__CLASS__, "cmp"));
    return $dates;
}
public static function cmp($label1, $label2) {
    return strtotime($label1->date)-strtotime($label2->date);
}
}