<?php
namespace app\models;

use Yii;
use yii\helpers\Html;

use yii\base\Model;

class RegisterForm extends Model {
    
    public $key1;
    public $email;
    public $password;
    public $password_repeat;
    public $name;
    public function rules() {
        return [
          [['key1','email','password','password_repeat','name'],'required', 'message' => 'Это поле обязательно к заполнению!'],  
            ['key1', 'checkKey', 'message' => 'Такого ключа не существует!'],
          ['email','email','message' => 'Некорректный e-mail адрес!'],
            
          ['email', 'uniqueMail','message'=>'Такой E-mail уже есть в базе данных!','skipOnEmpty' => false, 'skipOnError' => false],
          ['password', 'compare','message' => 'Пароли должны совпадать!']    
                
                        
        ];
    }
    public function checkKey($attribute, $params) {
      $userkey = Userkeys::findOne([
        'key_user' => Html::encode($this->key1),
]); 
        if ($userkey==null || $userkey->is_used==1) {
             $this->addError($attribute,'Такого ключа не существует!');
        }
    }
    public function uniqueMail($attribute, $params) {
        $mails = User::find()
    ->where(['email' => Html::encode($this->email)])
    ->count();
        if ($mails>0) {
             $this->addError($attribute,'Такой E-mail уже есть в базе данных!');
        }
    }
    public function validateRepit() {
        if (!empty($repitPassword) && ($repitPassword===$password)) {
            
        }
        
    }
    public function registrationUser() {
         $key = Html::encode($this->key1);
            $name = Html::encode($this->name);
            $email = Html::encode($this->email);
            $password = Html::encode($this->password);
            $userkey = Userkeys::findOne([
        'key_user' => $key,
        'is_used' => 0,
]);
        if ($this->validate()) {
            $user = new User();
            
            $user->email=$email;
            $hash = Yii::$app->getSecurity()->generatePasswordHash($password);
            $user->project_id=$userkey->project_id;
            $user->username=$name;
            $user->password=$hash;
            $user->save();
            $userkey->is_used=1;
            $userkey->save();
    }
}
}
?>


