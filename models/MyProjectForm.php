<?php

namespace app\models;


use Yii;
use yii\helpers\Html;
use yii\base\Model;

/**
 * LoginForm is the model behind the login form.
 *
 * @property User|null $user This property is read-only.
 *
 */
class MyProjectForm extends Model
{
    public $name;
    public $target;
    public $problems;
    public $describe;
    public $visibleMail = true;
    public $visibleAuthor = true;
    public $file;
    public $shortDescribe;
    public $linkVideo;
    public $authors;
    public $contactMail;
    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [
            // username and password are both required
            [['name','target', 'problems','describe','shortDescribe','authors','contactMail'], 'required'],
            // rememberMe must be a boolean value
            ['visibleMail', 'boolean'],
            ['linkVideo','string'],
            ['contactMail','email','message' => 'Некорректный e-mail адрес!'],
            [['file'],'image', 'extensions' => 'png, jpg',
        'minWidth' => 1000, 'maxWidth' => 1200,
        'minHeight' => 400, 'maxHeight' => 600,
    ],
            ['visibleAuthor', 'boolean'],
            ['shortDescribe','string', 'length' => [0, 300]]
            // password is validated by validatePassword()
            
        ];
    }
   public function getYoutubeIdFromUrl($url) {
    $parts = parse_url($url);
    if(isset($parts['query'])){
        parse_str($parts['query'], $qs);
        if(isset($qs['v'])){
            return $qs['v'];
        }else if(isset($qs['vi'])){
            return $qs['vi'];
        }
    }
    if(isset($parts['path'])){
        $path = explode('/', trim($parts['path'], '/'));
        return $path[count($path)-1];
    }
    return false;
}
    public function loadData($project) {
        $this->name = $project->name;
        $this->contactMail = $project->contact_mail;
        $this->authors = $project->authors;
        $this->shortDescribe = $project->short_describe;
        $this->target = $project->target;
        $this->problems = $project->problems;
        $this->describe = $project->describe_project;
        $file1 =  Html::img("/files/" . $project->image);
        
        $this->file = $file1;
        $this->visibleMail = $project->show_mail;
        if ($project->video_project!="") {
        $this->linkVideo =  "https://youtu.be/".$project->video_project;
        }
        $this->visibleAuthor = $project->show_author;
    }
    public function saveData($project) {
//      preg_match("#(?<=v=)[a-zA-Z0-9-]+(?=&)|(?<=v\/)[^&\n]+(?=\?)|(?<=v=)[^&\n]+|(?<=youtu.be/)[^&\n]+#", $linkVideo, $matches);
//        if(isset($matches[2]) && $matches[2] != ''){
//     $linkVideo = $matches[2];
//        }
//        if (Html::encode($this->getYoutubeIdFromUrl($this->linkVideo))=="") {
//$project->video_project = null;
//        }
        $project->contact_mail =  Html::encode($this->contactMail);
        $project->authors =  Html::encode($this->authors);
        $project->video_project = Html::encode($this->getYoutubeIdFromUrl($this->linkVideo));
        $project->name =  Html::encode($this->name);
        $project->target =  Html::encode($this->target);
        $project->short_describe =    Html::encode($this->shortDescribe);
        $project->problems =  Html::encode($this->problems);
        $project->describe_project =  Html::encode($this->describe);
        $project->show_mail =  (int)$this->visibleMail;
        $project->show_author = (int) $this->visibleAuthor;
        $project->save();
        
    }  

}
